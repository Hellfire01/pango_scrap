# toutes les fonctions gérant les erreurs

def critical_error (message, exception, exit_value = 2)
  puts 'Error :'.red
  puts message
  puts 'exception class   : ' + exception.class.to_s
  puts 'exception message : ' + exception.message
  if exit_value > 0
    exit exit_value
  end
end
