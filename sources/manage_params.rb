class Params
  attr_reader :days, :to
  
  def initialize()
    @days = 7
    @to = 1
    if ARGV.size != 0
      if ARGV[0] == 'help'
        puts 'pango_scrap [number of days] [jump]'
        puts '[number of days] allows to choose how many days are chosen, default : 7'
        puts '[jump] number of days to jump, default : 1 ( ignores today )'
        exit 0
      end
      @days = ARGV[0].to_i
      if @days <= 0
        puts 'Error :'.red
        puts 'the number of days cannot be <= 0 for first argument'
        exit 1
      end
      if ARGV.size == 2
        @to = ARGV[1].to_i
        if @to < 0
          puts 'Error :'.red
          puts 'the number of days cannot be < 0 for second argument'
          exit 1
        end
      end
    end
    puts ''
    @days = (@days + @to).day.ago.strftime('%Y-%m-%d')
    @to = to.day.ago.strftime('%Y-%m-%d')
    puts 'Thank you for using pango_scrap'.green
    puts 'counting from ' + days.yellow + ' to ' + to.yellow + ' included'
    puts ''
  end
end
