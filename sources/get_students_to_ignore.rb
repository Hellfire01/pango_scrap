class Get_students
  def get_students
    content = File.open(@file_name).read
    ret = []
    content.split("\n").each do |line|
      buff = line.strip
      if buff != ''
        ret << buff
      end
    end
    ret
  end
  
  def initialize(file_name)
    @file_name = file_name
  end
end
