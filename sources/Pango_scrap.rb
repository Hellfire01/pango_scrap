# the class that manages the programm
class Pango_scrap
  private
  # récupère toutes les events aux journées via les évents sur les pangs
  def get_data_of_student(student)
    ret = []
    begin
      page = Nokogiri::HTML(open(student.link)) do |nokogiri|
        nokogiri.noblanks.noerror
      end
    rescue StandardError => e
      puts '==================================================='
      puts 'Error : '.red + 'une exception a été lancée pour ' + student.email.yellow
      puts 'Nom de la classe : ' + e.class.to_s
      puts 'Message          : ' + e.message
      puts '==================================================='
      puts ''
      return []
    end
    page.xpath('/html/body/main/div/div/div/div/div/ul/li').each do |modifs|
      tmp = modifs.text.strip
      if tmp.include? 'Temps de présence'
        date = tmp.split(' ')[0].split('-')
        date = date[2] + '-' + date[1] + '-' + date[0]
        if date >= @from && date <= @to
          ret << tmp
        end
      end
    end
    ret
  end

  # récupère les liens + emails de tous les étudiants présent sur le site
  def get_links(page)
    ret = []
    page.xpath('//div[contains(@class, "student-container")]').each do |div|
      buff = Struct::Student.new
      buff.email = div.xpath('./strong').text
      buff.link = div.xpath('./div/a')[0]['href']
      unless @ignore.include? buff.email
        ret << buff
      end
    end
    ret
  end

  def get_score(events)
    count = 0
    events.each do |e|
      count += e.split(' ').last.to_f.round(1)
    end
    puts 'score = ' + count.round(1).to_s.green + ' => ' + count.round.to_s.magenta
  end

  public
  # lance la recherche
  def run
    begin
      page = Nokogiri::HTML(open(@link)) do |nokogiri|
        nokogiri.noblanks.noerror
      end
    rescue StandardError => e
      critical_error('could not access  : "' + @link.yellow + '"', e)
    end
    links = get_links(page)
    links.each do |student|
      ret = get_data_of_student(student)
      if ret.size != 0
        puts student.email.yellow
        ret.each do |diff|
          puts diff
        end
        get_score(ret)
        puts ''
      end
    end
  end

  # init de la classe avec les variables ARGV
  def initialize(link, days, to, ignore)
    @link = link
    @from = days
    @to = to
    @ignore = ignore
  end
end
