#!/usr/bin/env ruby
# coding: utf-8

require 'pp'
require 'open-uri'

def load_gem(gem)
  begin
    require gem
  rescue Exception => e
    puts ''
    puts "exception while trying to load #{gem}"
    puts 'message is : ' + e.messages
    puts ''
    exit 2
  end
end

# gems
load_gem 'colorize'
load_gem 'nokogiri'
load_gem 'rails'

require_relative 'sources/errors'
require_relative 'sources/Pango_scrap'
require_relative 'sources/get_students_to_ignore'
require_relative 'sources/manage_params'

Struct.new('Student', :link, :email)

params = Params.new
students_to_ignore = Get_students.new('to_ignore.txt')

scrap = Pango_scrap.new('https://samsung.absences.wac.epitech.eu/student/', params.days, params.to, students_to_ignore.get_students)
scrap.run
